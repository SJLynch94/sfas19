﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dropables : MonoBehaviour {

    public GameObject[] drops;

    public bool enemyDead = false;

    public void LateUpdate()
    {
        var r = Random.Range(1, 101);

        if(enemyDead)
        {
            Vector3 pos = new Vector3(transform.position.x, -2.75f, transform.position.z);
            foreach(GameObject drop in drops)
            {
                if(drop != null)
                {
                    if (r <= 60)
                    {
                        Instantiate(drop, pos, Quaternion.identity);
                    }
                }
            }
        }
    }
}
