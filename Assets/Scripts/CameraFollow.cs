﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // The Camera Target
    [SerializeField]
    Transform m_PlayerTransform;

    // The Z Distance from the Camera Target
    [SerializeField]
    float m_CameraDistanceZ = 15.0f;

    // Use this for initialization
    void Start ()
    {
		
	}


	
	// Update is called once per frame
	void Update ()
    {
        transform.position = new Vector3(m_PlayerTransform.position.x, transform.position.y, m_PlayerTransform.position.z - m_CameraDistanceZ);
        //RaycastHit[] hits;
        //hits = Physics.RaycastAll(transform.position, transform.forward, m_CameraDistanceZ);
        //foreach(var hit in hits)
        //{
        //    if(hit.transform.tag != "Player")
        //    {
        //        Renderer r = hit.collider.GetComponent<Renderer>();
        //        if(r != null)
        //        {
        //            AutoTransparency AT = r.GetComponent<AutoTransparency>();
        //            if(AT == null)
        //            {
        //                AT = r.gameObject.AddComponent<AutoTransparency>();
        //            }
        //            AT.BeTransparent();
        //        }
        //    }
        //}
	}
}
