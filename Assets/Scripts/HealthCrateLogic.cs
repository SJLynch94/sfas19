﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCrateLogic : MonoBehaviour {

    [SerializeField]
    int m_HealAmount = 25;

    void OnTriggerEnter(Collider other)
    {
        Health healing = other.GetComponentInChildren<Health>();

        if (healing.gameObject.tag == "Player")
        {
            if (healing)
            {
                healing.AddHealth(m_HealAmount);
                Destroy(gameObject);
            }
        }
    }
}
