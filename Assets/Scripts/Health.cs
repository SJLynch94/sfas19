﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    // The total health of this unit
    [SerializeField]
    int m_Health = 100;

    [SerializeField]
    int m_MaxHealth = 100;

    [SerializeField]
    int m_MinHealth = 0;

    UIManager m_UIManager;

    PlayerController m_PlayerController;
    AIController m_AIController;

    Dropables dropItems;

    Spawner m_Spawner;
    AISpawn m_AISpawn;

    DeathTrigger m_DTrigger;

    GunLogic m_GunLogic;

    public ParticleSystem mParticleSystem;
    float mParticleTimer;

    public int Lives
    {
        get { return mLivesCounter; }
        set { mLivesCounter = value; }
    }

    int mLivesCounter;
    const float MAX_RESPAWN_TIME = 1;
    float mRespawnTimer = MAX_RESPAWN_TIME;

    bool mAlive;

    public bool Alive
    {
        get { return mAlive; }
        set { mAlive = value; }
    }

    private void Start()
    {
        m_UIManager = FindObjectOfType<UIManager>();
        m_PlayerController = FindObjectOfType<PlayerController>() as PlayerController;
        dropItems = GetComponent<Dropables>();
        m_Spawner = FindObjectOfType<Spawner>();
        m_AISpawn = FindObjectOfType<AISpawn>();
        m_AIController = GetComponent<AIController>();

        m_GunLogic = FindObjectOfType<GunLogic>() as GunLogic;

        mParticleTimer = 1.0f;

        if (gameObject.tag == "Player")
        {
            mLivesCounter = 3;
            Alive = true;
        }


        if(m_UIManager)
        {
            if(gameObject.tag == "Player")
            {
                m_Health = Mathf.Clamp(m_Health, m_MinHealth, m_MaxHealth);
                m_UIManager.SetHealthText(m_Health);
            }
        }
    }

    private void Update()
    {
        if(gameObject.tag == "Player")
        {
            if (!gameObject.GetComponent<Health>().Alive)
            {
                UpdateRespawnDeathTimer();
                return;
            }
            else
            {
                StopSpawnEffect();
            }
        }
    }

    public void AddHealth(int h)
    {
        if(gameObject.tag == "Player")
        {
            m_Health += h;
            m_Health = Mathf.Clamp(m_Health, m_MinHealth, m_MaxHealth);
            m_UIManager.SetHealthText(m_Health);
        }
    }

    public void DoDamage(int damage)
    {
        m_Health -= damage;
        if (gameObject.tag == "Player")
        {
            m_Health = Mathf.Clamp(m_Health, m_MinHealth, m_MaxHealth);
            m_UIManager.SetHealthText(m_Health);
        }
        if (m_Health <= 0)
        {
            if (gameObject.tag == "Player")
            {
                if (m_PlayerController)
                {
                    if (Lives == 0)
                    {
                        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                    }
                    else
                    {
                        RespawnPlayer();
                    }
                }
            }
            if (gameObject.tag == "Enemy")
            {
                KillAI();
            }
        }
    }

    public void KillAI()
    {
        for (var i = 0; i < m_Spawner.m_SpawnIDList.Count; ++i)
        {
            if (m_Spawner.m_SpawnIDList[i] == m_AIController.SID)
            {
                m_Spawner.KillEnemy(m_Spawner.m_SpawnIDList[i]);
                Destroy(gameObject);
                dropItems.enemyDead = true;
                dropItems.LateUpdate();
            }
        }
    }

    public void PlayerHasDied()
    {
        Alive = false;
        gameObject.SetActive(false);
        mRespawnTimer = MAX_RESPAWN_TIME;
        UpdateRespawnDeathTimer();
        m_PlayerController.Die();
    }

    public void RespawnPlayer()
    {
        --Lives;
        PlaySpawnEffect(gameObject);
        if (m_GunLogic.m_BulletAmmo <= 0)
        {
            m_GunLogic.m_BulletAmmo = 25;
        }
        if (m_GunLogic.m_GrenadeAmmo <= 0)
        {
            m_GunLogic.m_GrenadeAmmo = 3;
        }
        if (GetHealth <= 0)
        {
            SetHealth(100);
        }
        m_UIManager.SetHealthText(m_Health);
        m_UIManager.SetAmmoText(m_GunLogic.m_BulletAmmo, m_GunLogic.m_GrenadeAmmo);
        Alive = true;
        //m_PlayerController.Alive = true;
        transform.position = m_PlayerController.m_SpawningPosition;
        transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
    }

    void PlaySpawnEffect(GameObject e)
    {
        mParticleSystem.transform.position = e.transform.position;
        while((mParticleTimer -= (Time.deltaTime * 2)) > 0)
        {
            mParticleSystem.Play();
        }
    }

    void StopSpawnEffect()
    {
        mParticleSystem.Stop();
    }

    public void UpdateRespawnDeathTimer()
    {
        if((mRespawnTimer -= (Time.deltaTime * 2)) < 0.0f)
        {
            RespawnPlayer();
        }
    }

    public bool IsAlive()
    {
        return m_Health > 0;
    }

    public int GetHealth
    {
        get { return m_Health; }
    }

    public void SetHealth(int h)
    {
        m_Health = h;
    }
}
