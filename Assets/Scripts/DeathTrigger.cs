﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTrigger : MonoBehaviour
{
    // --------------------------------------------------------------

    // Events
    public delegate void PlayerDeath();
    public static event PlayerDeath OnPlayerDeath;

    // --------------------------------------------------------------

    private int m_damage = 35;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerController playerController = other.gameObject.GetComponent<PlayerController>();
            Health healthComp = other.gameObject.GetComponent<Health>();
            if(playerController)
            {
                if (healthComp.GetHealth > 0)
                {
                    healthComp.DoDamage(m_damage);
                    if(healthComp.GetHealth <= 0)
                    {
                        healthComp.RespawnPlayer();
                    }
                }
                else
                {
                    healthComp.RespawnPlayer();
                }
            }
        }
        else if(other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<Health>().DoDamage(m_damage);
            Debug.Log(gameObject.tag + " has gone onto Lava Zone " + m_damage);
        }
    }
}
