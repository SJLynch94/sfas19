﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spawner : MonoBehaviour {

    public Color gizmosColour = Color.red;

    public enum ESpawnType
    {
        Survival,
        Once,
        Wave,
        TimedWave
    }

    public enum EEnemyLvl
    {
        Easy,
        Medium,
        Hard,
        Boss
    }

    public EEnemyLvl me_EnemyLevel;

    public GameObject m_EasyEnemy;
    public GameObject m_MediumEnemy;
    public GameObject m_HardEnemy;
    public GameObject m_BossEnemy;

    private Dictionary<EEnemyLvl, GameObject> m_Enemies = new Dictionary<EEnemyLvl, GameObject>(4);


    public int m_TotalEnemies = 10;
    private int m_NumEnemies = 0;
    private int m_SpawnedEnemies = 0;
    private int m_NumEnemiesModifier = 12;

    private int m_SpawnID;
    public List<int> m_SpawnIDList;

    private bool m_WaveSpawn = false;
    public bool mb_Spawn = true;
    public ESpawnType me_SpawnType;


    public float m_WaveTimer;
    private float m_TimeTillWave = 0.0f;


    public int m_TotalWaves;
    private int m_NumWaves = 0;

    private GameObject m_Floor;

    public Vector3 centre;
    public Vector3 size;

    AISpawn m_AISpawner;
    AIController m_AIController;

    ParticleSystem mParticleSystem;

    float mParticleTimer = 1;

    UIManager mUIManager;

    // Use this for initialization
    void Start () {
        m_AISpawner = GetComponent(typeof(AISpawn)) as AISpawn;
        m_Enemies.Add(EEnemyLvl.Easy, m_EasyEnemy);
        m_Enemies.Add(EEnemyLvl.Medium, m_MediumEnemy);
        m_Enemies.Add(EEnemyLvl.Hard, m_HardEnemy);
        m_Enemies.Add(EEnemyLvl.Boss, m_BossEnemy);

        me_SpawnType = (ESpawnType)PlayerPrefs.GetInt("me_SpawnType");
        m_TotalWaves = PlayerPrefs.GetInt("m_TotalWaves");
        me_EnemyLevel = (EEnemyLvl)PlayerPrefs.GetInt("me_EnemyLevel");
        m_WaveTimer = PlayerPrefs.GetFloat("m_WaveTimer");

        m_Floor = GameObject.FindGameObjectWithTag("Floor");

        m_SpawnIDList = new List<int>();

        mParticleSystem = GetComponentInChildren<ParticleSystem>() as ParticleSystem;

        mUIManager = FindObjectOfType<UIManager>() as UIManager;
        mUIManager.SetGameText(me_SpawnType.ToString(), me_EnemyLevel.ToString(), m_NumWaves, TimeTillWave);
    }
	
	// Update is called once per frame
	void Update () {
		if(mb_Spawn)
        {
            switch(me_SpawnType)
            {
                case ESpawnType.Survival:
                    {
                        if(m_NumEnemies == m_TotalEnemies)
                        {
                            return;
                        }
                        if(m_NumEnemies < m_TotalEnemies)
                        {
                            SpawnEnemy();
                        }
                        break;
                    }
                case ESpawnType.Once:
                    {
                        if(m_SpawnedEnemies >= m_TotalEnemies)
                        {
                            mb_Spawn = false;
                        }
                        else
                        {
                            SpawnEnemy();
                            mb_Spawn = true;
                        }
                        if(!mb_Spawn && m_NumEnemies == 0)
                        {
                            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                        }
                        break;
                    }
                case ESpawnType.Wave:
                    {
                        if(m_NumWaves < m_TotalWaves + 1)
                        {
                            if(m_WaveSpawn)
                            {
                                SpawnEnemy();
                            }
                            if(m_NumEnemies == 0)
                            {
                                ++m_NumWaves;
                                m_WaveSpawn = true;
                                m_TotalEnemies += m_NumEnemiesModifier;
                            }
                            if(m_NumEnemies == m_TotalEnemies)
                            {
                                m_WaveSpawn = false;
                            }
                        }
                        else
                        {
                            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                        }
                        break;
                    }
                case ESpawnType.TimedWave:
                    {
                        if(m_NumWaves <= m_TotalWaves)
                        {
                            TimeTillWave += Time.deltaTime;
                            if(m_WaveSpawn)
                            {
                                SpawnEnemy();
                            }
                            if(TimeTillWave >= m_WaveTimer)
                            {
                                ++m_NumWaves;
                                m_WaveSpawn = true;
                                TimeTillWave = 0.0f;
                                m_NumEnemies = 0;
                            }
                            if(m_NumEnemies >= m_TotalEnemies)
                            {
                                m_WaveSpawn = false;
                            }
                        }
                        else
                        {
                            mb_Spawn = false;
                        }
                        break;
                    }
            }
        }
        mUIManager.SetGameText(me_SpawnType.ToString(), me_EnemyLevel.ToString(), m_NumWaves, TimeTillWave);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = gizmosColour;
        Gizmos.DrawCube(transform.position, new Vector3(0.5f, 0.5f, 0.5f));
        Gizmos.DrawWireCube(centre, size);
    }

    private void SpawnEnemy()
    {
        ++m_NumEnemies;
        ++m_SpawnedEnemies;
        Vector3 point = new Vector3(Random.Range(-47.5f, 47.5f), -2.75f, Random.Range(-47.5f, 47.5f));
        GameObject e = Instantiate(m_Enemies[me_EnemyLevel], point, Quaternion.identity);
        PlaySpawnEffect(e);
        SetID();
        m_AISpawner.SetName(m_SpawnID);
        m_AIController = e.GetComponent<AIController>();
        m_AIController.SID = m_SpawnID;
    }

    void PlaySpawnEffect(GameObject e)
    {
        mParticleSystem.transform.position = e.transform.position;
        while((mParticleTimer -= (Time.deltaTime * 2)) > 0)
        {
            mParticleSystem.Play();
        }
    }

    void StopSpawnEffect()
    {
        mParticleSystem.Stop();
    }

    public void SetID()
    {
        m_SpawnID = Random.Range(1, 1000);
        if (!m_SpawnIDList.Contains(m_SpawnID))
        {
            m_SpawnIDList.Add(m_SpawnID);
        }
        else
        {
            SetID();
        }
    }

    public void KillEnemy(int sID)
    {
        for(var i = 0; i < m_SpawnIDList.Count; ++i)
        {
            if(sID == m_SpawnIDList[i])
            {
                --m_NumEnemies;
                m_SpawnIDList.Remove(sID);
            }
        }
    }

    public void EnableSpawner(int sID)
    {
        if(m_SpawnID == sID)
        {
            mb_Spawn = true;
        }
    }

    public void DisableSpawner(int sID)
    {
        if (m_SpawnID == sID)
        {
            mb_Spawn = false;
        }
    }

    public float TimeTillWave
    {
        get{ return m_TimeTillWave; }
        set { m_TimeTillWave = value; }
    }

    public void EnableTrigger()
    {
        mb_Spawn = true;
    }

    public int GetSpawnID
    {
        get{ return m_SpawnID; }
    }
}
