﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    // --------------------------------------------------------------

    [SerializeField]
    Text m_BulletText;

    [SerializeField]
    Text m_GrenadeText;

    [SerializeField]
    Text m_HealthText;

    [SerializeField]
    Text m_SpawnType;

    [SerializeField]
    Text m_Difficulty;

    [SerializeField]
    Text m_WaveCount;

    [SerializeField]
    Text m_WaveTimer;

    Spawner mSpawner;

    private void Awake()
    {
        mSpawner = FindObjectOfType<Spawner>() as Spawner;
        m_WaveCount.GetComponent<Text>().enabled = false;
        m_WaveTimer.GetComponent<Text>().enabled = false;
    }

    // --------------------------------------------------------------

    public void SetAmmoText(int bulletCount, int grenadeCount)
    {
        if(m_BulletText)
        {
            m_BulletText.text = "Bullets: " + bulletCount.ToString("0");
        }
        
        if(m_GrenadeText)
        {
            m_GrenadeText.text = "Grenades: " + grenadeCount.ToString("0");
        }
    }

    public void SetHealthText(int healthCount)
    {
        if(m_HealthText)
        {
            m_HealthText.text = "Health: " + healthCount.ToString("0");
        }
    }

    public void SetGameText(string type, string diff, int wavecount, float wavetimer)
    {
        if(m_SpawnType)
        {
            m_SpawnType.text = "Wave Type: " + type;
        }

        if (m_Difficulty)
        {
            m_Difficulty.text = "Difficulty: " + diff;
        }

        if(mSpawner.me_SpawnType == Spawner.ESpawnType.Wave || mSpawner.me_SpawnType == Spawner.ESpawnType.TimedWave)
        {
            m_WaveCount.GetComponent<Text>().enabled = true;
            if (m_WaveCount)
            {
                m_WaveCount.text = "Wave: " + wavecount.ToString("0");
            }
            if (mSpawner.me_SpawnType == Spawner.ESpawnType.TimedWave)
            {
                m_WaveTimer.GetComponent<Text>().enabled = true;
                if (m_WaveTimer)
                {
                    m_WaveTimer.text = "Wave Time: " + wavetimer.ToString("0");
                }
            }
        }   
    }
}
