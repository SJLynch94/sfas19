﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Health healthComp = other.gameObject.GetComponent<Health>();
            healthComp.RespawnPlayer();
        }
        else if (other.gameObject.tag == "Enemy")
        {
             Spawner mSpawner = FindObjectOfType<Spawner>() as Spawner;
             mSpawner.KillEnemy(other.gameObject.GetComponent<AIController>().SID);
             Destroy(other.gameObject);
        }
    }
}
