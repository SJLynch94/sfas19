﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    public int maxLavaZones = 30;
    private GameObject m_Floor;
    public GameObject m_Lava;


    private Vector3 originPosition;


    void Start()
    {
        m_Floor = GameObject.FindGameObjectWithTag("Floor");
        originPosition = m_Floor.transform.position;
        Spawn();

    }

    void Spawn()
    {
        for (int i = 0; i < maxLavaZones; i++)
        {
            //Vector3 randomPosition = new Vector3(Random.Range(-floor.transform.position.x / 2, floor.transform.position.x / 2), -3.4f, Random.Range(-floor.transform.position.z / 2, floor.transform.position.z / 2));
            Vector3 randomPosition = new Vector3(Random.Range(m_Floor.transform.position.x - m_Floor.transform.localScale.x / 2, m_Floor.transform.position.x + m_Floor.transform.
            localScale.x / 2), -2.75f, Random.Range(m_Floor.transform.position.z - m_Floor.transform.localScale.z / 2, m_Floor.transform.position.z + m_Floor.transform.localScale.z / 2));
            Instantiate(m_Floor, randomPosition, Quaternion.identity);
            originPosition = randomPosition;
        }
    }
}
