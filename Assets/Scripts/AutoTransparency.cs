﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoTransparency : MonoBehaviour {

    private Shader mOldShader = null;
    private Color mOldColour = Color.black;
    private float mTransparency = 0.3f;
    private const float mTargetTransparency = 0.3f;
    private const float mFallOff = 0.1f;

    public void BeTransparent()
    {
        mTransparency = mTargetTransparency;
        if(mOldShader == null)
        {
            mOldShader = GetComponent<Renderer>().material.shader;
            mOldColour = GetComponent<Renderer>().material.color;
            GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (mTransparency < 1.0f)
        {
            Color c = GetComponent<Renderer>().material.color;
            c.a = mTransparency;
            GetComponent<Renderer>().material.color = c;
        }
        else
        {
            GetComponent<Renderer>().material.shader = mOldShader;
            GetComponent<Renderer>().material.color = mOldColour;
            Destroy(this);
        }
        mTransparency += ((1.0f - mTargetTransparency) * Time.deltaTime) / mFallOff;
	}
}
