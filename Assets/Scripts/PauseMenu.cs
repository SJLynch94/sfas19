﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public static bool bIsGamePaused = false;

    public GameObject mPauseUIManager;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(bIsGamePaused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
	}

    public void PauseGame()
    {
        mPauseUIManager.SetActive(true);
        Time.timeScale = 0.0f;
        bIsGamePaused = true;
    }

    public void ResumeGame()
    {
        mPauseUIManager.SetActive(false);
        Time.timeScale = 1.0f;
        bIsGamePaused = false;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadMainMenu()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
}
