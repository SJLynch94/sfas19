﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    private int waveCount = 1;
    private int spawnType;
    private int diff;
    private float timer = 60.0f;

    public OptionsManager opManager;

    private void Start()
    {
        //opManager = FindObjectOfType<OptionsManager>();
        waveCount = PlayerPrefs.GetInt("m_TotalWaves");
        spawnType = PlayerPrefs.GetInt("me_SpawnType");
        diff = PlayerPrefs.GetInt("eEnemyLevel");
        timer = PlayerPrefs.GetFloat("m_WaveTimer");

        opManager.SetOptionsText(waveCount, timer);
    }

    public void PlayGame()
    {
        var r = Random.Range(1, 2);
        //SceneManager.LoadScene(r, LoadSceneMode.Single);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void TryAgainGoToMainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void IncreaseWave()
    {
        ++waveCount;
        PlayerPrefs.SetInt("m_TotalWaves", waveCount);
        opManager.SetOptionsText(waveCount, timer);
    }

    public void DecreaseWave()
    {
        --waveCount;
        PlayerPrefs.SetInt("m_TotalWaves", waveCount);
        if (waveCount <= 1)
        {
            waveCount = 1;
            PlayerPrefs.SetInt("m_TotalWaves", waveCount);
        }
        opManager.SetOptionsText(waveCount, timer);
    }

    public void IncreaseTime()
    {
        ++timer;
        PlayerPrefs.SetFloat("m_WaveTimer", timer);
        opManager.SetOptionsText(waveCount, timer);
    }

    public void DecreaseTime()
    {
        --timer;
        PlayerPrefs.SetFloat("m_WaveTimer", timer);
        if (timer <= 1)
        {
            timer = 1.0f;
            PlayerPrefs.SetFloat("m_WaveTimer", timer);
        }
        opManager.SetOptionsText(waveCount, timer);
    }

    public void ChangeDiff(int x)
    {
        if (x == 0)
        {
            PlayerPrefs.SetInt("me_EnemyLevel", x);
        }

        if (x == 1)
        {
            PlayerPrefs.SetInt("me_EnemyLevel", x);
        }

        if (x == 2)
        {
            PlayerPrefs.SetInt("me_EnemyLevel", x);
        }

        if (x == 3)
        {
            PlayerPrefs.SetInt("me_EnemyLevel", x);
        }
    }

    public void ChangeType(int x)
    {
        if(x == 0)
        {
            PlayerPrefs.SetInt("me_SpawnType", x);
        }

        if (x == 1)
        {
            PlayerPrefs.SetInt("me_SpawnType", x);
        }

        if (x == 2)
        {
            PlayerPrefs.SetInt("me_SpawnType", x);
        }

        if (x == 3)
        {
            PlayerPrefs.SetInt("me_SpawnType", x);
        }
    }



}
