﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour {

    [SerializeField]
    Text m_WaveCount;

    [SerializeField]
    Text m_WaveTimer;

    public void SetOptionsText(int waves, float waveTimer)
    {
        if (m_WaveTimer)
        {
            m_WaveTimer.text = waveTimer.ToString("0");
        }

        if(m_WaveCount)
        {
            m_WaveCount.text = waves.ToString("0");
        }
    }
}
